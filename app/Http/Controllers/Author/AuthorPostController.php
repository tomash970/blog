<?php

namespace App\Http\Controllers\Author;

use App\Author;
use App\Http\Controllers\ApiController;
use App\Post;
use App\Transformers\PostTransformer;
use App\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AuthorPostController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PostTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:manage-posts')->except(['index']);
        $this->middleware('can:view,author')->only(['index']);
        $this->middleware('can:post,author')->only(['store']);
        $this->middleware('can:edit-post,author')->only(['update']);
        $this->middleware('can:delete-post,author')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Author $author)
    {
        if (request()->user()->tokenCan('read-general') || request()->user()->tokenCan('manage-posts')) {
            $posts = $author->posts;

            return $this->showAll($posts);
        }
        throw new AuthorizationException('Invalid scope(s)');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $author)
    {
        $rules = [
            'title'      => 'required',
            'content'    => 'required',
            'attachment' => 'required|image'
        ];

        $this->validate($request, $rules);

        $data = $request->all();

        $data['status'] = Post::UNAUTHORISED_POST;
        $data['attachment'] = $request->attachment->store('');
        $data['author_id'] = $author->id;

        $post = Post::create($data);

        return $this->showOne($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Author $author, Post $post)
    {
        $rules = [
            
            'content'    => 'required',
            'status'      => 'in:' . Post::AUTHORISED_POST . ',' . Post::UNAUTHORISED_POST,
            'attachment' => 'image',
        ];

        $this->validate($request, $rules);

        $this->checkAuthor($author, $post);

        $post->fill($request->only([
            'title',
            'content',

        ]));

        if ($request->has('status')) {
            $post->status = $request->status;

            if ($post->isAuthorised() && $post->categories()->count() == 0) {
                return $this->errorResponse('An active post must have at least one category', 409);
            }
        }

        if ($request->hasFile('attachment')) {
            Storage::delete($post->attachment);
            $post->attachment = $request->attachment->store('');
        }

        if ($post->isClean()) {
            return $this->errorResponse('You need to enter some values to update!', 422);
        }

        $post->save();

        return $this->showOne($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy(Author $author, Post $post)
    {
        $this->checkAuthor($author, $post);

        Storage::delete($post->attachment);

        $post->delete();

        return $this->showOne($post);
    }

    protected function checkAuthor(Author $author, Post $post)
    {
        if ($author->id != $post->author_id) {
            throw new HttpException(422, "The specified author is not the actual author of the post!");
            
        }
    }
}
