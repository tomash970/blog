<?php

namespace App\Http\Controllers\Author;

use App\Author;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class AuthorVisitorController extends ApiController
{
    
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Author $author)
    {
        $this->allowedAdminAction();
        $visitors = $author->posts()->whereHas('comments')->with('comments.visitor')->get()->pluck('comments')->collapse()->pluck('visitor')->unique('id')->values();

        return $this->showAll($visitors);
    }

}
