<?php

namespace App\Http\Controllers\Post;

use App\Category;
use App\Http\Controllers\ApiController;
use App\Post;
use Illuminate\Http\Request;

class PostCategoryController extends ApiController
{
    public function __construct()
    {
        $this->middleware('client.credentials')->only(['index']);
        $this->middleware('auth:api')->except(['index']);
        $this->middleware('scope:manage-posts')->except(['index']);
        $this->middleware('can:add-category,post')->only(['update']);
        $this->middleware('can:delete-category,post')->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Post $post)
    {
        $categories = $post->categories;
        return $this->showAll($categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post, Category $category)
    {
        $post->categories()->syncWithoutDetaching([$category->id]);
        return $this->showAll($post->categories);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post, Category $category)
    {
        if (!$post->categories()->find($category->id)) {
            return $this->errorResponse('The specified category in not a category of this post!', 404);
        }
        $post->categories()->detach($category->id);

        return $this->showAll($post->categories);
    }
}
