<?php

namespace App\Http\Controllers\Post;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class PostVisitorController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Post $post)
    {
        $this->allowedAdminAction();
        $visitors = $post->comments()->with('visitor')->get()->pluck('visitor')->values();
        return $this->showAll($visitors);
    }

   
}
