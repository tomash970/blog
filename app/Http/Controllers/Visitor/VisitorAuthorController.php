<?php

namespace App\Http\Controllers\Visitor;

use App\Http\Controllers\ApiController;
use App\Post;
use App\Visitor;
use Illuminate\Http\Request;

class VisitorAuthorController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Visitor $visitor)
    {
        $this->allowedAdminAction();
        $authors = $visitor->comments()->with('post.author')->get()->pluck('post.author')->unique('id')->values();
        return $this->showAll($authors);
    }

    
}
