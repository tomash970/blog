<?php

namespace App\Http\Controllers\Visitor;

use App\Http\Controllers\ApiController;
use App\Visitor;
use Illuminate\Http\Request;

class VisitorController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('scope:read-general')->only(['show']);//juam ima i scome middlewareu index što nije bilo u prijašnjim lekciama nego show???
        $this->middleware('can:view,visitor')->only(['show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();
        $visitors = Visitor::has('comments')->get();
        return $this->showAll($visitors);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Visitor $visitor)
    {
        return $this->showOne($visitor);
    }

}
