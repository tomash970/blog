<?php

namespace App;

use App\Author;
use App\Category;
use App\Comment;
use App\Transformers\PostTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    const AUTHORISED_POST = 'authorised';
    const UNAUTHORISED_POST = 'unauthorised';

    public $transformer = PostTransformer::class;

    protected $fillable = [
    	'title',
    	'content',
    	'attachment',
    	'status',
    	'author_id'
    ];

    protected $hidden = [
        'pivot'
    ];

    public function isAuthorised(){
    	return $this->status == Post::AUTHORISED_POST;
    }

    public function categories()
    {
    	return $this->belongsToMany(Category::class);
    }

    public function author()
    {
    	return $this->belongsTo(Author::class);
    }

    public function comments()
    {
    	return $this->hasMany(Comment::class);
    }
}
