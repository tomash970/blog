<?php

namespace App;

use App\Post;
use App\Transformers\CommentTransformer;
use App\Visitor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;

    public $transformer = CommentTransformer::class;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'title',
    	'content',
    	'post_id',
    	'visitor_id'
    ];

    public function visitor()
    {
    	return $this->belongsTo(Visitor::class);
    }

    public function post()
    {
    	return $this->belongsTo(Post::class);
    }
}
