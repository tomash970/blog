<?php

namespace App\Policies;

use App\Traits\AdminActions;
use App\User;
use App\Visitor;
use Illuminate\Auth\Access\HandlesAuthorization;

class VisitorPolicy
{
    use HandlesAuthorization, AdminActions;



    /**
     * Determine whether the user can view the visitor.
     *
     * @param  \App\User  $user
     * @param  \App\Visitor  $visitor
     * @return mixed
     */
    public function view(User $user, Visitor $visitor)
    {
        return $user->id === $visitor->id;
    }

    /**
     * Determine whether the user can comment posts.
     *
     * @param  \App\User  $user
     * @param  \App\Visitor  $visitor
     * @return mixed
     */
    public function commentPost(User $user, Visitor $visitor)
    {
        return $user->id === $visitor->id;
    }

}
