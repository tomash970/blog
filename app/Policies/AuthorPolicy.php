<?php

namespace App\Policies;

use App\Author;
use App\Traits\AdminActions;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AuthorPolicy
{
    use HandlesAuthorization, AdminActions;

    /**
     * Determine whether the user can view the author.
     *
     * @param  \App\User  $user
     * @param  \App\Author  $author
     * @return mixed
     */
    public function view(User $user, Author $author)
    {
        return $user->id === $author->id;
    }

    /**
     * Determine whether the user can post the Post.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function post(User $user, Author $author)
    {
        return $user->id === $author->id;
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param  \App\User  $user
     * @param  \App\Author  $author
     * @return mixed
     */
    public function editPost(User $user, Author $author)
    {
        return $user->id === $author->id;
    }

    /**
     * Determine whether the user can delete the author.
     *
     * @param  \App\User  $user
     * @param  \App\Author  $author
     * @return mixed
     */
    public function deletePost(User $user, Author $author)
    {
        return $user->id === $author->id;
    }


}
