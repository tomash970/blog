<?php

namespace App\Providers;

use App\Author;
use App\Comment;
use App\Policies\AuthorPolicy;
use App\Policies\CommentPolicy;
use App\Policies\PostPolicy;
use App\Policies\UserPolicy;
use App\Policies\VisitorPolicy;
use App\Post;
use App\User;
use App\Visitor;
use Carbon\Carbon;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Visitor::class => VisitorPolicy::class,
        Author::class => AuthorPolicy::class,
        User::class => UserPolicy::class,
        Post::class => PostPolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin-action', function($user) {
            return $user->isAdmin();
        });



        Passport::routes();
        Passport::tokensExpireIn(Carbon::now()->addMinutes(60));
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));
        Passport::enableImplicitGrant();

        Passport::tokensCan([
            'comment-posts' => 'Create comment on specific post',
            'manage-posts'  => 'CRUD posts',
            'manage-account'  => 'Read your account data, id, name, email, if verified, ad if admin(cannot read password). Modify your account data (email, and password).',
            'read-general'  => 'Read general information like commenting categories, commenting posts, posting, posting categories, jour comments and posts',
        ]);
    }
}
