<?php

namespace App;

use App\Comment;
use App\Scopes\VisitorScope;
use App\Transformers\VisitorTransformer;



class Visitor extends User
{
    
    public $transformer = VisitorTransformer::class;

    protected static function boot()
    {
    	parent::boot();
    	static::addGlobalScope(new VisitorScope);
    }

    public function comments()
    {
    	return $this->hasMany(Comment::class);
    }

    
}
