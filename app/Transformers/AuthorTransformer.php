<?php

namespace App\Transformers;

use App\Author;
use League\Fractal\TransformerAbstract;

class AuthorTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Author $author)
    {
        return [
            'identifier'   => (int)$author->id,
            'name'         => (string)$author->name,
            'email'        => (string)$author->email,
            'isVerified'   => (int)$author->verified,
            'creationDate' => (string)$author->created_at,
            'lastChange'   => (string)$author->updated_at,
            'deletedDate'  => isset($author->deleted_at) ? (string)  $author->deleted_at : null,
            'links'        => [
                [
                'rel'  => 'self',
                'href' => route('authors.show', $author->id)
                ],
                [
                'rel'  => 'author.posts',
                'href' => route('authors.posts.index', $author->id)
                ],
                [
                'rel'  => 'author.categories',
                'href' => route('authors.categories.index', $author->id)
                ],
                [
                'rel'  => 'author.comments',
                'href' => route('authors.comments.index', $author->id)
                ],
                [
                'rel'  => 'author.visitors',
                'href' => route('authors.visitors.index', $author->id)
                ],
                [
                'rel'  => 'author.user',
                'href' => route('users.show', $author->id)
                ],
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier'   => 'id',
            'name'         => 'name',
            'email'        => 'email',
            'isVerified'   => 'verified',
            'creationDate' => 'created_at',
            'lastChange'   => 'updated_at',
            'deletedDate'  => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
          'id'         => 'identifier',       
          'name'       => 'name',       
          'email'      => 'email',       
          'verified'   => 'isVerified',       
          'created_at' => 'creationDate',                        
          'updated_at' => 'lastChange',                        
          'deleted_at' => 'deletedDate',                        
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
