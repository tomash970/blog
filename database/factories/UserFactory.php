<?php

use App\Author;
use App\Category;
use App\Comment;
use App\Post;
use App\User;
use App\Visitor;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
    	//static $password;
        'name'               => $faker->name,
        'email'              => $faker->unique()->safeEmail,
        'password'           => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token'     => str_random(10),
        'verified'           => $verified = $faker->randomElement([User::VERIFIED_USER, User::UNVERIFIED_USER]),
        'verification_token' => $verified == User::VERIFIED_USER ? null : User::generateVerificationCode(),
        'admin'              => $faker->randomElement(([User::ADMIN_USER, User::REGULAR_USER])),
    ];
});

//'password' => $password ?: $password = bcrypt('secret'),


$factory->define(Category::class, function (Faker $faker) {
    return [
        'name'        => $faker->word,
        'description' => $faker->paragraph(1),    
    ];
});


$factory->define(Post::class, function (Faker $faker) {
    return [
        'title'      => $faker->word,
        'content'    => $faker->paragraph(1),
        'attachment' => $faker->randomElement(['1.jpg', '2.jpg', '3.jpg']),
        'status'     => $faker->randomElement([Post::AUTHORISED_POST, Post::UNAUTHORISED_POST]),
        'author_id'  => User::all()->random()->id,
   
    ];
});

$factory->define(Comment::class, function (Faker $faker) {
	$author  = Author::has('posts')->get()->random();
	$visitor = User::all()->except($author->id)->random();

    return [
        'title'      => $faker->word,
        'content'    => $faker->paragraph(1),
        'visitor_id' => $visitor->id,
        'post_id'    => $author->posts->random()->id,
   
    ];
});


